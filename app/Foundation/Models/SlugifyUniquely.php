<?php

namespace App\Foundation\Models;

use Illuminate\Support\Str;

trait SlugifyUniquely
{
    public function getSluggableName()
    {
        if (method_exists($this, 'getSluggable')) {
            return $this->getSluggable();
        }

        return $this->name;
    }

    public function getSlugPropertyName()
    {
        if (property_exists($this, 'slugProperty')) {
            return $this->slugProperty;
        }

        return 'slug';
    }

    public function slugify()
    {
        $sluggable = $this->getSluggableName();
        $slugProperty = $this->getSlugPropertyName();

        $slug = Str::slug($sluggable);

        $count = self::whereRaw($slugProperty . " RLIKE '^" . $slug . "(-[0-9]+)?$'")->count();

        return $count ? "{$slug}-{$count}" : $slug;
    }

    public static function bootSlugifyUniquely()
    {
        static::saving(function($item){
            $slugProperty = $item->getSlugPropertyName();

            if (is_null($item->{$slugProperty})) {
                $item->{$slugProperty} = $item->slugify();
            }
        });
    }
}
