<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Review extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'rating' => $this->rating,
            'review' => $this->review,
            $this->mergeWhen(is_null($this->user), [
                'guest_name' => $this->guest_name,
            ]),
            $this->mergeWhen(!is_null($this->user), [
                'author' => new User($this->user),
            ]),
        ];
    }
}
