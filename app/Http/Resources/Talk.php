<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Talk extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'description' => $this->description,
            //'speaker' => $this->speaker->name,
            //'speaker' => new Speaker($this->speaker),
        ];
    }
}
