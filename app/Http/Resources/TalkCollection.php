<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class TalkCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection,
            'links' => [
                'self' => route('events.talks.index', [$request->event]),
            ],
        ];
    }

    public function with($request)
    {
        return [
            'meta' => [
                'talk' => 'PHPNE/LeedsPHP/Confoo',
            ],
            'presentation' => 'API Development with Laravel',
        ];
    }
}
