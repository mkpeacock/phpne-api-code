<?php

namespace App\Http\Controllers\Api;

use App\Review;
use App\Event;
use App\Talk;
use App\Http\Requests\CreateReviewRequest;
use App\Http\Resources\ReviewCollection as ReviewCollectionResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EventTalkReviewController extends Controller
{
    public function __construct(Review $reviewRepository)
    {
        $this->reviewRepository = $reviewRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function index(Event $event, Talk $talk)
    {
        //dd($talk->reviews->first()->rating);
        return new ReviewCollectionResource($talk->reviews);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function store(CreateReviewRequest $request, Event $event, Talk $talk)
    {
        $review = $this->reviewRepository->newInstance();
        $review->talk_id = $talk->id;
        $review->rating = ($request->rating > 5) ? 5 : $request->rating;
        $review->rating = ($request->rating < 0) ? 0 : $request->rating;
        $review->review = $request->review;
        if (is_null($request->user) && $request->has('guest_name') && $request->has('guest_email')) {
            $review->guest_name = $request->guest_name;
            $review->guest_email = $request->guest_email;
        }
        if (!is_null($request->user)) {
            $review->user_id = $user->id;
        }

        $review->save();

        return $review;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Event  $event
     * @param  \App\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event, Review $review)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Event  $event
     * @param  \App\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Event $event, Review $review, Talk $talk)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Event  $event
     * @param  \App\Review  $review
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event, Review $review, Talk $talk)
    {
        //
    }
}
