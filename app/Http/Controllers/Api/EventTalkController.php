<?php

namespace App\Http\Controllers\Api;

use App\Talk;
use App\Event;
use App\Http\Requests\CreateTalkRequest;
use App\Http\Resources\Talk as TalkResource;
use App\Http\Resources\TalkCollection as TalkCollectionResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EventTalkController extends Controller
{
    public function __construct(Talk $talkRepository)
    {
        $this->talkRepository = $talkRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function index(Event $event)
    {
        return new TalkCollectionResource($event->talks()->paginate());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function store(CreateTalkRequest $request, Event $event)
    {
        $talk = $this->talkRepository->newInstance();
        $talk->name = $request->name;
        $talk->description = $request->description;
        $talk->speaker_id = $request->speaker_id;
        $talk->event_id = $event->id;
        $talk->save();

        return $talk;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Event  $event
     * @param  \App\Talk  $talk
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event, Talk $talk)
    {
        return new TalkResource($talk);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Event  $event
     * @param  \App\Talk  $talk
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Event $event, Talk $talk)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Event  $event
     * @param  \App\Talk  $talk
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event, Talk $talk)
    {
        //
    }
}
