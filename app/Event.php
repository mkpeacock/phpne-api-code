<?php

namespace App;

use App\Foundation\Models\SlugifyUniquely;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends Model
{
    use SlugifyUniquely, SoftDeletes;

    protected $dates = [
        'start',
        'end',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function talks()
    {
        return $this->hasMany(Talk::class);
    }

    public function speakers()
    {
        return $this->hasManyThrough(Speaker::class, App\Talk::class);
    }

    public function sponsors()
    {
        return $this->belongsToMany(Sponsor::class, 'event_sponsors');
    }
}
