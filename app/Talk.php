<?php

namespace App;

use App\Foundation\Models\SlugifyUniquely;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Talk extends Model
{
    use SlugifyUniquely, SoftDeletes;

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function speaker()
    {
        return $this->belongsTo(Speaker::class);
    }

    public function event()
    {
        return $this->belongsTo(Event::class);
    }

    public function reviews()
    {
        return $this->hasMany(Review::class);
    }
}
