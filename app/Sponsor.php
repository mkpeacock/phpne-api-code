<?php

namespace App;

use App\Foundation\Models\SlugifyUniquely;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sponsor extends Model
{
    use SlugifyUniquely, SoftDeletes;

    protected $dates = [
        'started_sponsoring',
        'stopped_sponsoring',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function events()
    {
        return $this->belongsToMany(Event::class, 'event_sponsors');
    }
}
