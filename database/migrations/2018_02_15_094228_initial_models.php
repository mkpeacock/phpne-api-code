<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitialModels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug')->unique();
            $table->dateTime('start');
            $table->dateTime('end');
            $table->longText('description');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('sponsors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug')->unique();
            $table->string('email');
            $table->string('website');
            // Potential limitation, loss of data for sponsors who start/stop
            // However, primary purpose for v1 is if they should display on the home page
            // / sponsors page. Former sponsors will still display on the event page(s)
            $table->dateTime('started_sponsoring');
            $table->dateTime('stopped_sponsoring')->nullable()->default(null);
            $table->longText('description');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('speakers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug')->unique();
            $table->string('email');
            $table->string('website')->nullable()->default(null);
            $table->longText('biography');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('event_sponsors', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('event_id')->unsigned();
            $table->foreign('event_id')->references('id')->on('events');
            $table->integer('sponsor_id')->unsigned();
            $table->foreign('sponsor_id')->references('id')->on('sponsors');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('talks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug')->unique();
            $table->integer('event_id')->unsigned();
            $table->foreign('event_id')->references('id')->on('events');
            // Limitation: one speaker per event.
            $table->integer('speaker_id')->unsigned();
            $table->foreign('speaker_id')->references('id')->on('speakers');
            $table->longText('description');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('rating');
            $table->text('review');
            $table->string('guest_name')->nullable()->default(null);
            $table->string('guest_email')->nullable()->default(null);
            $table->integer('user_id')->unsigned()->nullable()->default(null);
            $table->foreign('user_id')->references('id')->on('users');
            // Limitation: currently its only talks that are reviewed
            // Maybe we want to change this to a polymorphic relationship
            // where talks and events can be reviewed
            $table->integer('talk_id')->unsigned();
            $table->foreign('talk_id')->references('id')->on('talks');
            $table->dateTime('approved_at')->nullable()->default(null);
            $table->softDeletes();
            $table->timestamps();
            $table->index(['talk_id', 'approved_at', 'deleted_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
