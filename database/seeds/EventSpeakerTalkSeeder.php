<?php

use Illuminate\Database\Seeder;

class EventSpeakerTalkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        $numSponsors = \App\Sponsor::count();

        // Make 50 events
        factory(App\Event::class, 50)->create()->each(function ($e) use ($faker, $numSponsors) {
            // Each with at least 1 talk
            $numTalks = $faker->numberBetween(1, 3);
            for ($i = 0; $i < $numTalks; $i++) {
                // TODO: re-use the odd speaker?
                $e->talks()->save(factory(App\Talk::class)->make(['event_id' => null]));
            }

            if ($numSponsors > 0) {
                $numSponsorsToAssociate = $faker->numberBetween(1, $numSponsors);
                for ($i = 0; $i < $numSponsorsToAssociate; $i++) {
                    $e->sponsors()->attach(\App\Sponsor::inRandomOrder()->first());
                }
            }
       });
    }
}
