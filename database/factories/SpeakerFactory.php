<?php

use Faker\Generator as Faker;

$factory->define(App\Speaker::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'website' => $faker->url,
        'biography' => $faker->paragraph,
    ];
});
