<?php

use Faker\Generator as Faker;

$factory->define(App\Talk::class, function (Faker $faker) {
    return [
        'name' => $faker->sentence,
        'description' => $faker->paragraph,
        'speaker_id' => function () {
            return factory(App\Speaker::class)->create()->id;
        },
        'event_id' => function () {
            return factory(App\Event::class)->create()->id;
        }
    ];
});
