<?php

use Faker\Generator as Faker;

$factory->define(App\Event::class, function (Faker $faker) {
    return [
        'name' => $faker->sentence,
        'description' => $faker->paragraph,
        'start' => $faker->dateTimeBetween('-3 years', '+2 month'),
        'end' => function ($data) {
            $end = clone $data['start'];
            $end->add(new \DateInterval('PT90M'));

            return $end;
        }
    ];
});
