<?php

use Faker\Generator as Faker;

$factory->define(App\Sponsor::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'email' => $faker->safeEmail,
        'website' => $faker->url,
        'description' => $faker->paragraph,
        'started_sponsoring' => $faker->dateTimeBetween('-3 years', 'now'),
        'stopped_sponsoring' => null,
    ];
});
