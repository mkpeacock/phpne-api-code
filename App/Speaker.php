<?php

namespace App;

use App\Foundation\Models\SlugifyUniquely;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Speaker extends Model
{
    use SlugifyUniquely, SoftDeletes;

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function talks()
    {
        return $this->hasMany(Talk::class);
    }

    public function events()
    {
        return $this->hasManyThrough(Event::class, Talk::class);
    }
}
